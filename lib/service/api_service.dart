import 'package:http/http.dart' as http;
import '../model/employee.dart';
import '../utils/parser/parser.dart';

class EmployeeService {

  Future<List<Employee>> fetchEmployeeFromApi() async {

    final requestUrl = _getRequestUrl();

    try {
      final response = await http
          .get(requestUrl, headers: {'content-type': 'application/json'});
      if (response.statusCode == 200) {
        var employees = response.body;
        return employees != null ? _parseEmployees(employees) : List<Employee>();
      } else {
        /// Here based on the error code (response.statusCode), we need to handle
        /// the error message. For the time being it is just thrown.
        throw ("Error occured: ${response.statusCode}" + response.reasonPhrase);
      }
    } catch (exception) {
      throw ("Exception occurred " + exception.toString());
    }
  }

  /// Parse response body and convert it to Employee object
  List<Employee> _parseEmployees(String responseBody) {
    List<dynamic> parsedData = Parser().parseJsonStringFrom(responseBody);

    ///We need to parse the data to the corresponding objects.
    var result = parsedData != null && parsedData.isNotEmpty
        ? parsedData.map((employee) => _toEmployee(employee)).toList()
        : [];
    return result;
  }

  String _getRequestUrl() {
    return 'http://www.mocky.io/v2/5d565297300000680030a986';
  }

  /// converting each Employees to Models
  Employee _toEmployee(Map<String, dynamic> map) {
    return Employee.fromJson(map);
  }
}
