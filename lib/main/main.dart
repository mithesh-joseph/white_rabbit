import 'package:flutter/material.dart';
import '../module/startup/startup.dart';
import '../manager/app_manager.dart';

void main() async {
  await AppManager.instance.initialize();
  return runApp(StartupView());
}