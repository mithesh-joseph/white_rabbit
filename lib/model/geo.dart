class Geo {
  String lat;
  String lng;

  Geo();

  Geo.fromJson(Map<String, dynamic> json)
      : lat = json["lat"],
        lng = json["lng"] {}

  Map<String, dynamic> toJson() {
    return {
      'lat': lat,
      'lng': lng,
    };
  }
}
