import '../model/geo.dart';

class Address {
  String street;
  String suite;
  String city;
  String zipcode;
  Geo geo;

  Address();
  Address.fromJson(Map<String, dynamic> json)
      : street = json["street"],
        suite = json["suite"],
        city = json["city"],
        zipcode = json["zipcode"] {
    geo = json["geo"] != null && json["geo"].isNotEmpty
        ? Geo.fromJson(json["geo"])
        : null;
  }

  Map<String, dynamic> toJson() {
    return {
      'street': street,
      'suite': suite,
      'city': city,
      'zipcode': zipcode,
      'geo': (geo != null) ? geo.toJson() : null,
    };
  }
}
