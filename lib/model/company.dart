class Company {
  String name;
  String catchPhrase;
  String bs;

  Company();

  Company.fromJson(Map<String, dynamic> json)
      : name = json["name"],
        catchPhrase = json["catchPhrase"],
        bs = json["bs"] {}

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      "catchPhrase": catchPhrase,
      "bs": bs,
    };
  }
}
