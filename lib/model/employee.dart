import 'address.dart';
import 'company.dart';
import '../utils/parser/parser.dart';

class Employee {
  String id;
  String name;
  String username;
  String email;
  String profile_image;
  Address address;
  Company company;

  Employee();

  Employee.fromJson(Map<String, dynamic> json)
      : id = json["id"].toString(),
        name = json["name"],
        username = json["username"],
        email = json["email"],
        profile_image = json["profile_image"] {
    address = json["address"] != null && json["address"].isNotEmpty
        ? Address.fromJson(json["address"])
        : null;
    company = json["company"] != null && json["company"].isNotEmpty
        ? Company.fromJson(json["company"])
        : null;
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      "username": username,
      'email': email,
      'profile_image': profile_image,
//      'address': address,
//      'company': company
    };
  }
}
