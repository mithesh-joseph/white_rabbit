import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:sqflite/sqlite_api.dart';

class DatabaseMigrator {
  /// Create a database with its config file name
  DatabaseMigrator(this.name);

  /// Database name
  final String name;

  /// The database scrips info map
  Map<String, dynamic> _info;

  /// Initialize the migrator
  Future<void> initialize() async {
    var data = await rootBundle.loadString('lib/assets/database/$name.json');
    _info = json.decode(data);
  }

  /// Creates the tables
  onCreate(Database db, int version) async {
    Batch batch = db.batch();
    List tables = this._info['onCreate'];
    tables.forEach((sql) {
      batch.execute(sql);
    });

    return await batch.commit(noResult: true);
  }

  /// Upgrades the database to a new version
  Future<void> onUpgrade(Database db, int oldVersion, int newVersion) async {
    Map migrations = this._info['onUpgrade'];
    Batch batch = db.batch();
    for (var idx = oldVersion + 1; idx <= newVersion; idx++) {
      List statements = migrations[idx.toString()];
      if (statements != null && statements.isNotEmpty) {
        statements.forEach((sql) {
          batch.execute(sql);
        });
      }
    }

    await batch.commit(noResult: true);

    Batch secondBatch = db.batch();

    await secondBatch.commit(noResult: true);
  }
}