import 'package:sqflite/sqflite.dart';
import 'irepository.dart';
import 'sql_data_context.dart';

abstract class EntityRepository<T> implements IRepository<T> {
  /// Name of table
  String get table;

  /// Deserialize
  T fromJson(Map map);

  /// Serialize
  Map toJson(T entity);

  /// Identity
  String entityId(T entity);

  /// The reference of database if needed by subclasses
  Future<Database> get database async => SQLiteDataContext().database;

  @override
  Future<List<T>> find(String query, [List queryArgs]) async {
    final db = await database;
    var results = await db.rawQuery(query, queryArgs);
    return results != null && results.isNotEmpty
        ? results.map((c) => fromJson(c)).toList()
        : null;
  }

  @override
  Future<bool> insert(List<T> notes) async {
    if (notes == null || notes.isEmpty) return false;
    final Database db = await database;
    var batch = db.batch();
    for (T note in notes) {
      batch.insert(table, toJson(note),
          conflictAlgorithm: ConflictAlgorithm.replace);
    }
    await batch.commit(noResult: true);
    return true;
  }

  @override
  Future<bool> update(List<T> notes) async {
    if (notes == null || notes.isEmpty) return false;
    final Database db = await database;
    var batch = db.batch();
    for (T contact in notes) {
      batch.update(table, toJson(contact),
          where: 'id=?', whereArgs: [entityId(contact)]);
    }
    await batch.commit(noResult: true);
    return true;
  }

  @override
  Future<bool> delete(List<T> notes) async {
    if (notes == null || notes.isEmpty) return false;
    final db = await database;
    var batch = db.batch();
    for (T entity in notes) {
      batch.delete(table, where: 'id=?', whereArgs: [entityId(entity)]);
    }
    await batch.commit(noResult: true);
    return true;
  }

  @override
  Future<bool> deleteOne(String entityId) async {
    final db = await database;
    var affectedRows =
    await db.delete(table, where: "id = ?", whereArgs: [entityId]);
    return affectedRows != null && affectedRows > 0 ? true : false;
  }

  @override
  Future<int> truncate() async {
    final db = await database;
    return await db.rawDelete('DELETE FROM $table');
  }

  // This can be used to check whether the employee exist in the db
  Future<bool> entityExists(T entity) async {
    final db = await database;
    final result = await db.rawQuery(
        'SELECT id FROM $table WHERE id = ?', [entityId(entity)]);
    return result?.length ?? 0 > 0;
  }
}
