import 'package:path/path.dart' as path;
import 'db_migrator.dart';
import 'package:sqflite/sqflite.dart';

const String databaseName = 'WhiteRabbit_V1';

///
/// Application uses this single store

class SQLiteDataContext {
  /// Singleton instance
  static final SQLiteDataContext __sharedInstance =
  SQLiteDataContext.__internal();

  factory SQLiteDataContext() {
    return __sharedInstance;
  }

  SQLiteDataContext.__internal();

  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    /// Database migrator
    final migrator = DatabaseMigrator(databaseName);

    /// Initialize the migrator
    await migrator.initialize();

    // Set the path to the database.
    var databasePath = path.join(await getDatabasesPath(), '$databaseName.db');

    // if _database is null we instantiate it
    _database = await openDatabase(databasePath,

        /// When the database is first created, create all tables
        onCreate: migrator.onCreate,

        /// On upgrade
        onUpgrade: migrator.onUpgrade,

        /// Set the version. This executes the onCreate function and provides a
        /// path to perform database upgrades and downgrades.
        version: 1,

        /// Single instance of database
        singleInstance: true);
    return _database;
  }

  Future<void> closeDatabase() async {
    final db = await database;
    await db.close();
    _database = null;
  }
}
