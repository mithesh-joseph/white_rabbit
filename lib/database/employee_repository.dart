import 'package:white_rabbit/model/employee.dart';
import 'entity_repository.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'sql_data_context.dart';

class EmployeeRepository extends EntityRepository<Employee> {
  /// Database context is as singleton instance of SQLiteDataContext
  Future<Database> get database async {
    return SQLiteDataContext().database;
  }

  @override
  String entityId(Employee entity) => entity.id;

  @override
  Employee fromJson(Map map) => Employee.fromJson(map);

  @override
  String get table => 'Employee';

  @override
  Map toJson(Employee entity) => entity.toJson();

  @override
  Future<bool> insert(List<Employee> entities) async {
    // Return early
    if (entities == null || entities.isEmpty) return Future.value(false);

    try {
      return _insertEmployees(entities);
    } catch (e) {
      throw ('Unable to save the employees. data invalid');
    }
  }

  /// Insert Employees
  Future<bool> _insertEmployees(List<Employee> employees) async {
    // Return early
    if (employees == null && employees.isEmpty) return Future.value(false);

    // database
    final db = await database;

    // a transaction
    var batch = db.batch();

    for (var employee in employees) {
      _insertEmployee(employee, batch);
    }

    // committing the transaction
    var result = await batch.commit(noResult: true);
    return result != null ? true : false;
  }

  /// Insert Employees one by one
  ///
  /// Uses a batch to commit all at once
  void _insertEmployee(Employee employee, Batch batch) {
    // Insert just the conversation
    batch.insert('Employee', employee.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);

//    if(employee.address != null) {
//
//      //employee.address.employeeId = employee.id;
//      batch.insert('Address', employee.address.toJson(),
//          conflictAlgorithm: ConflictAlgorithm.replace);
//    }
  }

  Future<List<Employee>> getEmployeesFromDb() async {
    var query =
        "SELECT * FROM $table";
    var results = await find(query);
    return results != null && results.isNotEmpty ? results : null;
  }

  Future<Employee> getEmployeeById(String employeeId) async {
    var queryArgs = [employeeId];
    var query = "SELECT * FROM $table WHERE id = ?;";
    var employee = await find(query, queryArgs);
    return employee != null && employee.isNotEmpty ? employee.first : null;
  }
}
