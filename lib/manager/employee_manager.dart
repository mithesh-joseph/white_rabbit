import 'package:flutter/foundation.dart';

import '../model/employee.dart';
import '../model/address.dart';
import '../model/company.dart';
import '../database/employee_repository.dart';
import '../service/api_service.dart';

class EmployeeManager with ChangeNotifier {
  ///Creating singleton instance for the EmployeeManager
  static final EmployeeManager __sharedInstance = EmployeeManager.internal();

  factory EmployeeManager() {
    return __sharedInstance;
  }

  EmployeeManager.internal();

  // Cache
  final _cache = Map<String, Employee>();

  final _repository = EmployeeRepository();

  /// Fetching Employees form api and parse the result ot the Employee Model.
  Future<List<Employee>> fetchEmployees() async {

    /// First check in the database (Here instead of checking cache first,
    /// we are checking db)
    var employees = await getEmployeesFromDb();
    if(employees != null && employees.isNotEmpty) return employees;


    ///Then checking for cache values. It is not the actual way.
    ///But here since we need data from db first, we are just considering cache
    ///as second.
    getEmployeesFromCache();

    employees = await EmployeeService().fetchEmployeeFromApi();
    if (employees != null && employees.isNotEmpty) {
      await _processEntities(employees);
    }
    /// Add to cache
    _setEmployees(employees);
  }

  /// Process the data received by api request and
  /// update to database
  Future<void> _processEntities(List<Employee> employees) async {
    if (employees == null || employees.isEmpty) return;
    await _processRecords(employees);
  }

  Future<void> _processRecords(List<Employee> employees) async {
    if (employees == null) return;
    await _repository.insert(employees);
  }

  /// Clear and repopulate cache
  _setEmployees(List<Employee> employees, {bool notify = true}) {
    _cache.clear();
    employees.forEach((c) {
      _cache[c.id] = c;
    });

    _notifyChanges(notify: notify);
  }

  /// Notify the changes to the listeners
  _notifyChanges({bool notify = true}) {
    /// Get al the cached employees
    final employees = _cache.values.toList();

    if (notify) {
      notifyListeners();
    }
  }

  /// Check if a conversation exists
  List<Employee> getEmployeesFromCache() {
    /// Check in cache first
    return _cache != null ? _cache.values.toList() : null;
  }

  Future<List<Employee>> getEmployeesFromDb() async{
    return _repository.getEmployeesFromDb();
  }

  Future<Employee> getEmployeeForId(String userId) async{
    return _repository.getEmployeeById(userId);
  }
}
