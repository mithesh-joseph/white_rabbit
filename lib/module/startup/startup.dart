import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../manager/employee_manager.dart';
import '../../configuration/config.dart';
import '../../manager/app_manager.dart';
import '../../configuration/page_route.dart' as PR;
import '../../module/employee/employee_list.dart';
import '../../module/employee/employee_details.dart';

class StartupView extends StatefulWidget {
  StartupView();

  @override
  State<StatefulWidget> createState() {
    return StartupViewState();
  }
}

class StartupViewState extends State<StartupView> {
  @override
  void initState() {
    super.initState();
    AppManager.instance.setInitialRoute(PR.employeeList);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<EmployeeManager>.value(
          value: EmployeeManager(),
        )
      ],
      child: MaterialApp(
        builder: (BuildContext context, Widget child) {
          return MediaQuery(
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: child);
        },
        title: AppConfig.appName,

        /// instead of conventional '/' root [AppManager]
        /// is expected to provide initial route based on the configured
        /// routes from the values of [routes]
        initialRoute: AppManager.instance.initialRoute,

        routes: {
          PR.employeeList: (context) => EmployeeList(),
          PR.employeeDetails: (context) => EmployeeDetails(),
        },
        theme: ThemeData.fallback(),
      ),
    );
  }
}
