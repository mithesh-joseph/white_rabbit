import 'package:flutter/material.dart';
import 'package:white_rabbit/manager/employee_manager.dart';
import 'package:white_rabbit/model/employee.dart';
import 'package:provider/provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:white_rabbit/module/employee/employee_details.dart';
import '../../configuration/page_route.dart' as PR;

class EmployeeList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return EmployeeListState();
  }
}

class EmployeeListState extends State<EmployeeList> with ChangeNotifier {
  List<Employee> _employees;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      left: true,
      top: true,
      right: true,
      bottom: true,
      child: Scaffold(
        backgroundColor: Colors.black,
        appBar: _buildAppBar(),
        body: _mainContainer(),
      ),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text('White Rabbit Employee List'),
    );
  }

  Widget _mainContainer() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Colors.grey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          RaisedButton(
            child: Text('Get data from API or DB'),
            onPressed: () async {
              _employees = await EmployeeManager().fetchEmployees();
              setState(() {});
            },
          ),
          Container(
            height: _employees.length *
                60.0, //For the time being it is given as this.
            child: ListView.builder(
              reverse: true,
              itemCount: _employees.length,
              itemBuilder: (context, index) {
                return Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: _listViewTile(_employees[index]));
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _listViewTile(Employee employee) {
    return InkWell(
      child: Container(
        height: 60, //just given as manually. Need to change it
        color: Colors.green,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _getProfileImage(employee),
            Column(
              children: <Widget>[
                _getName(employee),
                //_getCompanyName(employee),
              ],
            )
          ],
        ),
      ),
      onTap: () async {
        Navigator.push(
          context,
          MaterialPageRoute(
            settings: RouteSettings(name: PR.employeeDetails),
            builder: (context) => EmployeeDetails(
              employee: employee,
            ),
          ),
        );
      },
    );
  }

  Widget _getProfileImage(Employee employee) {
    return employee.profile_image != null
        ? CachedNetworkImage(
            fit: BoxFit.cover,
            imageUrl: employee.profile_image,
            placeholder: (context, url) => Center(
              child: SizedBox(
                width: 60.0,
                height: 60.0,
                child: new CircularProgressIndicator(),
              ),
            ),
            errorWidget: (context, url, error) => IconButton(
              icon: Icon(Icons.file_download),
              onPressed: () {},
            ),
          )
        : Container(
            color: Colors.amber,
            height: 60,
            width: 60,
          );
  }

  Widget _getName(Employee employee) {
    return Padding(
        padding: EdgeInsets.only(left: 20), child: Text(employee.name));
  }

  Widget _getCompanyName(Employee employee) {
    return Padding(
        padding: EdgeInsets.only(left: 20), child: Text(employee.company.name));
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    final employees =
        Provider.of<EmployeeManager>(context).getEmployeesFromCache();
    if (employees != null) {
      _employees = employees;
    } else {
      Future.delayed(
        Duration(milliseconds: 500),
        () {
          //_showNoProductAvailableAlert();
        },
      );
    }
  }
}
