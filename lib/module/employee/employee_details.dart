import 'package:flutter/material.dart';
import 'package:white_rabbit/manager/employee_manager.dart';
import 'package:white_rabbit/model/employee.dart';
import 'package:provider/provider.dart';
import 'package:cached_network_image/cached_network_image.dart';

class EmployeeDetails extends StatefulWidget {
  final Employee employee;

  EmployeeDetails({
    Key key,
    @required this.employee,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return EmployeeDetailsState();
  }
}

class EmployeeDetailsState extends State<EmployeeDetails> with ChangeNotifier {
  Employee _employee;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      left: true,
      top: true,
      right: true,
      bottom: true,
      child: Scaffold(
        backgroundColor: Colors.black,
        appBar: _buildAppBar(),
        body: _mainContainer(),
      ),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text('White Rabbit Employee details'),
    );
  }

  Widget _mainContainer() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Colors.grey,
      child: _DetailViewContainer(),
    );
  }

  Widget _DetailViewContainer() {
    String address;
    String company;
    if (widget.employee.address != null) {
      address = widget.employee.address.street +
          widget.employee.address.city +
          widget.employee.address.zipcode +
          widget.employee.address.suite;
    }
    if (widget.employee.company != null) {
      company = widget.employee.company.name +
          widget.employee.company.catchPhrase +
          widget.employee.company.bs;
    }

    return Padding(
      padding: EdgeInsets.all(20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _getProfileImage(widget.employee),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(widget.employee.name != null ? widget.employee.name : ''),
                Text(widget.employee.username != null
                    ? widget.employee.username
                    : ''),
                Text(
                    widget.employee.email != null ? widget.employee.email : ''),
                Text(address != null ? address : ''),
                Text(company != null ? company : ''),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _getProfileImage(Employee employee) {
    return employee.profile_image != null
        ? CachedNetworkImage(
            fit: BoxFit.cover,
            imageUrl: employee.profile_image,
            placeholder: (context, url) => Center(
              child: SizedBox(
                width: 60.0,
                height: 60.0,
                child: new CircularProgressIndicator(),
              ),
            ),
            errorWidget: (context, url, error) => IconButton(
              icon: Icon(Icons.file_download),
              onPressed: () {},
            ),
          )
        : Container(
            color: Colors.amber,
            height: 60,
            width: 60,
          );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}
